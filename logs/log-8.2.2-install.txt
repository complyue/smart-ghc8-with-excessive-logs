===--- building phase 0
gmake --no-print-directory -f ghc.mk phase=0 phase_0_builds
gmake[1]: Nothing to be done for 'phase_0_builds'.
===--- building phase 1
gmake --no-print-directory -f ghc.mk phase=1 phase_1_builds
gmake[1]: Nothing to be done for 'phase_1_builds'.
===--- building final phase
gmake --no-print-directory -f ghc.mk phase=final install
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
"rm" -f                                         "/opt/local/ghc8.2.2/bin/hp2ps"  
create () { touch "$1" && chmod 755 "$1" ; } && create                                           "/opt/local/ghc8.2.2/bin/hp2ps"
echo '#!/bin/sh'                                       >> "/opt/local/ghc8.2.2/bin/hp2ps"
echo 'exedir="/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"'                    >> "/opt/local/ghc8.2.2/bin/hp2ps"
echo 'exeprog="hp2ps"'                          >> "/opt/local/ghc8.2.2/bin/hp2ps"
echo 'executablename="$exedir/$exeprog"'           >> "/opt/local/ghc8.2.2/bin/hp2ps"
echo 'datadir="/opt/local/ghc8.2.2/share"'                             >> "/opt/local/ghc8.2.2/bin/hp2ps"
echo 'bindir="/opt/local/ghc8.2.2/bin"'                               >> "/opt/local/ghc8.2.2/bin/hp2ps"
echo 'topdir="/opt/local/ghc8.2.2/lib/ghc-8.2.2"'                               >> "/opt/local/ghc8.2.2/bin/hp2ps"
cat utils/hp2ps/hp2ps.wrapper                         >> "/opt/local/ghc8.2.2/bin/hp2ps"
chmod +x                                         "/opt/local/ghc8.2.2/bin/hp2ps"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
"rm" -f                                 "/opt/local/ghc8.2.2/bin/ghci-8.2.2"  
create () { touch "$1" && chmod 755 "$1" ; } && create                                   "/opt/local/ghc8.2.2/bin/ghci-8.2.2"
echo '#!/bin/sh'                               >> "/opt/local/ghc8.2.2/bin/ghci-8.2.2"
echo 'exec "/opt/local/ghc8.2.2/bin/ghc-8.2.2" --interactive "$@"' >> "/opt/local/ghc8.2.2/bin/ghci-8.2.2"
chmod +x                                 "/opt/local/ghc8.2.2/bin/ghci-8.2.2"
"rm" -f "/opt/local/ghc8.2.2/bin/ghci"  
ln -s ghci-8.2.2 "/opt/local/ghc8.2.2/bin/ghci"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/." && /opt/local/bin/ginstall -c -m 644  includes/./*.h "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/./" &&   /opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/rts" && /opt/local/bin/ginstall -c -m 644  includes/rts/*.h "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/rts/" &&   /opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/rts/prof" && /opt/local/bin/ginstall -c -m 644  includes/rts/prof/*.h "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/rts/prof/" &&   /opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/rts/storage" && /opt/local/bin/ginstall -c -m 644  includes/rts/storage/*.h "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/rts/storage/" &&   /opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/stg" && /opt/local/bin/ginstall -c -m 644  includes/stg/*.h "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/stg/" &&  true
/opt/local/bin/ginstall -c -m 644  includes/ghcautoconf.h includes/ghcplatform.h includes/ghcversion.h includes/dist-derivedconstants/header/DerivedConstants.h "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include"
/opt/local/bin/ginstall -c -m 644  rts/dist/build/ffi.h rts/dist/build/ffitarget.h "/opt/local/ghc8.2.2/lib/ghc-8.2.2/include/"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
"rm" -f                                         "/opt/local/ghc8.2.2/bin/hsc2hs"  
create () { touch "$1" && chmod 755 "$1" ; } && create                                           "/opt/local/ghc8.2.2/bin/hsc2hs"
echo '#!/bin/sh'                                       >> "/opt/local/ghc8.2.2/bin/hsc2hs"
echo 'exedir="/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"'                    >> "/opt/local/ghc8.2.2/bin/hsc2hs"
echo 'exeprog="hsc2hs"'                          >> "/opt/local/ghc8.2.2/bin/hsc2hs"
echo 'executablename="$exedir/$exeprog"'           >> "/opt/local/ghc8.2.2/bin/hsc2hs"
echo 'datadir="/opt/local/ghc8.2.2/share"'                             >> "/opt/local/ghc8.2.2/bin/hsc2hs"
echo 'bindir="/opt/local/ghc8.2.2/bin"'                               >> "/opt/local/ghc8.2.2/bin/hsc2hs"
echo 'topdir="/opt/local/ghc8.2.2/lib/ghc-8.2.2"'                               >> "/opt/local/ghc8.2.2/bin/hsc2hs"
echo 'HSC2HS_EXTRA="--cflag=-m64 --cflag=-fno-stack-protector --lflag=-m64"' >> "/opt/local/ghc8.2.2/bin/hsc2hs"
cat utils/hsc2hs/hsc2hs.wrapper                         >> "/opt/local/ghc8.2.2/bin/hsc2hs"
chmod +x                                         "/opt/local/ghc8.2.2/bin/hsc2hs"
/opt/local/bin/ginstall -c -m 644  utils/hsc2hs/template-hsc.h "/opt/local/ghc8.2.2/lib/ghc-8.2.2"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
"rm" -f                                         "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"  
create () { touch "$1" && chmod 755 "$1" ; } && create                                           "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
echo '#!/bin/sh'                                       >> "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
echo 'exedir="/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"'                    >> "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
echo 'exeprog="ghc-pkg"'                          >> "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
echo 'executablename="$exedir/$exeprog"'           >> "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
echo 'datadir="/opt/local/ghc8.2.2/share"'                             >> "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
echo 'bindir="/opt/local/ghc8.2.2/bin"'                               >> "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
echo 'topdir="/opt/local/ghc8.2.2/lib/ghc-8.2.2"'                               >> "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
cat utils/ghc-pkg/ghc-pkg.wrapper                         >> "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
chmod +x                                         "/opt/local/ghc8.2.2/bin/ghc-pkg-8.2.2"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
"rm" -f "/opt/local/ghc8.2.2/bin/ghc-pkg"  
ln -s ghc-pkg-8.2.2 "/opt/local/ghc8.2.2/bin/ghc-pkg"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
"rm" -f                                         "/opt/local/ghc8.2.2/bin/hpc"  
create () { touch "$1" && chmod 755 "$1" ; } && create                                           "/opt/local/ghc8.2.2/bin/hpc"
echo '#!/bin/sh'                                       >> "/opt/local/ghc8.2.2/bin/hpc"
echo 'exedir="/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"'                    >> "/opt/local/ghc8.2.2/bin/hpc"
echo 'exeprog="hpc"'                          >> "/opt/local/ghc8.2.2/bin/hpc"
echo 'executablename="$exedir/$exeprog"'           >> "/opt/local/ghc8.2.2/bin/hpc"
echo 'datadir="/opt/local/ghc8.2.2/share"'                             >> "/opt/local/ghc8.2.2/bin/hpc"
echo 'bindir="/opt/local/ghc8.2.2/bin"'                               >> "/opt/local/ghc8.2.2/bin/hpc"
echo 'topdir="/opt/local/ghc8.2.2/lib/ghc-8.2.2"'                               >> "/opt/local/ghc8.2.2/bin/hpc"
cat utils/hpc/hpc.wrapper                         >> "/opt/local/ghc8.2.2/bin/hpc"
chmod +x                                         "/opt/local/ghc8.2.2/bin/hpc"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
"rm" -f                                         "/opt/local/ghc8.2.2/bin/runghc-8.2.2"  
create () { touch "$1" && chmod 755 "$1" ; } && create                                           "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
echo '#!/bin/sh'                                       >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
echo 'exedir="/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"'                    >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
echo 'exeprog="runghc"'                          >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
echo 'executablename="$exedir/$exeprog"'           >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
echo 'datadir="/opt/local/ghc8.2.2/share"'                             >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
echo 'bindir="/opt/local/ghc8.2.2/bin"'                               >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
echo 'topdir="/opt/local/ghc8.2.2/lib/ghc-8.2.2"'                               >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
echo 'ghcprog="ghc-8.2.2"' >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
cat utils/runghc/runghc.wrapper                         >> "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
chmod +x                                         "/opt/local/ghc8.2.2/bin/runghc-8.2.2"
"rm" -f "/opt/local/ghc8.2.2/bin/runhaskell"  
ln -s runghc "/opt/local/ghc8.2.2/bin/runhaskell"
"rm" -f "/opt/local/ghc8.2.2/bin/runghc"  
ln -s runghc-8.2.2 "/opt/local/ghc8.2.2/bin/runghc"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
"rm" -f                                         "/opt/local/ghc8.2.2/bin/ghc-8.2.2"  
create () { touch "$1" && chmod 755 "$1" ; } && create                                           "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
echo '#!/bin/sh'                                       >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
echo 'exedir="/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"'                    >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
echo 'exeprog="ghc-stage2"'                          >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
echo 'executablename="$exedir/$exeprog"'           >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
echo 'datadir="/opt/local/ghc8.2.2/share"'                             >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
echo 'bindir="/opt/local/ghc8.2.2/bin"'                               >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
echo 'topdir="/opt/local/ghc8.2.2/lib/ghc-8.2.2"'                               >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
echo 'executablename="$exedir/ghc"' >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
cat ghc/ghc.wrapper                         >> "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
chmod +x                                         "/opt/local/ghc8.2.2/bin/ghc-8.2.2"
"rm" -f "/opt/local/ghc8.2.2/bin/ghc"  
ln -s ghc-8.2.2 "/opt/local/ghc8.2.2/bin/ghc"
#  driver/ghc-usage.txt driver/ghci-usage.txt includes/dist-derivedconstants/header/platformConstants settings = libraries to install
#  "/opt/local/ghc8.2.2/lib/ghc-8.2.2" = directory to install to
#
# The .dll case calls STRIP_CMD explicitly, instead of `install -s`, because
# on Win64, "install -s" calls a strip that doesn't understand 64bit binaries.
# For some reason, this means the DLLs end up non-executable, which means
# executables that use them just segfault.
/opt/local/bin/ginstall -c -m 755 -d  "/opt/local/ghc8.2.2/lib/ghc-8.2.2"
for i in  driver/ghc-usage.txt driver/ghci-usage.txt includes/dist-derivedconstants/header/platformConstants settings; do case $i in *.a) /opt/local/bin/ginstall -c -m 644  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2"; true  "/opt/local/ghc8.2.2/lib/ghc-8.2.2"/`basename $i` ;; *.dll) /opt/local/bin/ginstall -c -m 755  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2" ; strip  "/opt/local/ghc8.2.2/lib/ghc-8.2.2"/`basename $i` ;; *.so) /opt/local/bin/ginstall -c -m 755  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2" ;; *.dylib) /opt/local/bin/ginstall -c -m 755  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2";; *) /opt/local/bin/ginstall -c -m 644  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2"; esac; done
gcc -E -undef -traditional -P -DINSTALLING -DLIB_DIR='"/opt/local/ghc8.2.2/lib/ghc-8.2.2"' -DINCLUDE_DIR='"/opt/local/ghc8.2.2/lib/ghc-8.2.2/include"' -DFFI_INCLUDE_DIR= -DFFI_LIB_DIR= '-DFFI_LIB="Cffi"' -x c -Iincludes -Iincludes/dist -Iincludes/dist-derivedconstants/header -Iincludes/dist-ghcconstants/header rts/package.conf.in -o rts/dist/package.conf.install.raw
grep -v '^#pragma GCC' rts/dist/package.conf.install.raw | sed -e 's/""//g' -e 's/:[ 	]*,/: /g' >rts/dist/package.conf.install
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"
for i in utils/unlit/dist/build/tmp/unlit utils/hp2ps/dist/build/tmp/hp2ps utils/hp2ps/dist/build/tmp/hp2ps utils/hsc2hs/dist-install/build/tmp/hsc2hs utils/hsc2hs/dist-install/build/tmp/hsc2hs utils/ghc-pkg/dist-install/build/tmp/ghc-pkg utils/ghc-pkg/dist-install/build/tmp/ghc-pkg utils/hpc/dist-install/build/tmp/hpc utils/hpc/dist-install/build/tmp/hpc utils/runghc/dist-install/build/tmp/runghc utils/runghc/dist-install/build/tmp/runghc ghc/stage2/build/tmp/ghc-stage2 ghc/stage2/build/tmp/ghc-stage2 iserv/stage2/build/tmp/ghc-iserv iserv/stage2_p/build/tmp/ghc-iserv-prof iserv/stage2_dyn/build/tmp/ghc-iserv-dyn; do \
	/opt/local/bin/ginstall -c -m 755  $i "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"; \
done
"mv" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-stage2" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2"
"rm" -rf "/opt/local/ghc8.2.2/lib/ghc-8.2.2/package.conf.d"  
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/package.conf.d"
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts"
#  rts/dist/build/libHSrts.a rts/dist/build/libHSrts_p.a rts/dist/build/libHSrts-ghc8.2.2.so rts/dist/build/libHSrts_l.a rts/dist/build/libHSrts_debug.a rts/dist/build/libHSrts_thr.a rts/dist/build/libHSrts_thr_debug.a rts/dist/build/libHSrts_thr_l.a rts/dist/build/libHSrts_thr_p.a rts/dist/build/libHSrts_debug-ghc8.2.2.so rts/dist/build/libHSrts_thr-ghc8.2.2.so rts/dist/build/libHSrts_thr_debug-ghc8.2.2.so rts/dist/build/libHSrts_l-ghc8.2.2.so rts/dist/build/libHSrts_thr_l-ghc8.2.2.so rts/dist/build/libffi.so rts/dist/build/libffi.so.6 rts/dist/build/libffi.so.6.0.4 rts/dist/build/libCffi.a rts/dist/build/libCffi_p.a rts/dist/build/libCffi_l.a rts/dist/build/libCffi_debug.a rts/dist/build/libCffi_thr.a rts/dist/build/libCffi_thr_debug.a rts/dist/build/libCffi_thr_l.a rts/dist/build/libCffi_thr_p.a = libraries to install
#  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts" = directory to install to
#
# The .dll case calls STRIP_CMD explicitly, instead of `install -s`, because
# on Win64, "install -s" calls a strip that doesn't understand 64bit binaries.
# For some reason, this means the DLLs end up non-executable, which means
# executables that use them just segfault.
/opt/local/bin/ginstall -c -m 755 -d  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts"
for i in  rts/dist/build/libHSrts.a rts/dist/build/libHSrts_p.a rts/dist/build/libHSrts-ghc8.2.2.so rts/dist/build/libHSrts_l.a rts/dist/build/libHSrts_debug.a rts/dist/build/libHSrts_thr.a rts/dist/build/libHSrts_thr_debug.a rts/dist/build/libHSrts_thr_l.a rts/dist/build/libHSrts_thr_p.a rts/dist/build/libHSrts_debug-ghc8.2.2.so rts/dist/build/libHSrts_thr-ghc8.2.2.so rts/dist/build/libHSrts_thr_debug-ghc8.2.2.so rts/dist/build/libHSrts_l-ghc8.2.2.so rts/dist/build/libHSrts_thr_l-ghc8.2.2.so rts/dist/build/libffi.so rts/dist/build/libffi.so.6 rts/dist/build/libffi.so.6.0.4 rts/dist/build/libCffi.a rts/dist/build/libCffi_p.a rts/dist/build/libCffi_l.a rts/dist/build/libCffi_debug.a rts/dist/build/libCffi_thr.a rts/dist/build/libCffi_thr_debug.a rts/dist/build/libCffi_thr_l.a rts/dist/build/libCffi_thr_p.a; do case $i in *.a) /opt/local/bin/ginstall -c -m 644  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts"; true  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts"/`basename $i` ;; *.dll) /opt/local/bin/ginstall -c -m 755  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts" ; strip  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts"/`basename $i` ;; *.so) /opt/local/bin/ginstall -c -m 755  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts" ;; *.dylib) /opt/local/bin/ginstall -c -m 755  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts";; *) /opt/local/bin/ginstall -c -m 644  $i  "/opt/local/ghc8.2.2/lib/ghc-8.2.2/rts"; esac; done
"inplace/bin/ghc-cabal" copy libraries/ghc-prim dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/ghc-prim-0.5.1.1
"inplace/bin/ghc-cabal" copy libraries/integer-gmp dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/integer-gmp-1.0.1.0
"inplace/bin/ghc-cabal" copy libraries/base dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/base-4.10.1.0
"inplace/bin/ghc-cabal" copy libraries/filepath dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/filepath-1.4.1.2
"inplace/bin/ghc-cabal" copy libraries/array dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/array-0.5.2.0
"inplace/bin/ghc-cabal" copy libraries/deepseq dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/deepseq-1.4.3.0
"inplace/bin/ghc-cabal" copy libraries/bytestring dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/bytestring-0.10.8.2
"inplace/bin/ghc-cabal" copy libraries/containers dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/containers-0.5.10.2
"inplace/bin/ghc-cabal" copy libraries/time dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/time-1.8.0.2
"inplace/bin/ghc-cabal" copy libraries/unix dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/unix-2.7.2.2
"inplace/bin/ghc-cabal" copy libraries/directory dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/directory-1.3.0.2
"inplace/bin/ghc-cabal" copy libraries/process dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/process-1.6.1.0
"inplace/bin/ghc-cabal" copy libraries/hpc dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/hpc-0.6.0.3
"inplace/bin/ghc-cabal" copy libraries/pretty dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/pretty-1.1.3.3
"inplace/bin/ghc-cabal" copy libraries/binary dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/binary-0.8.5.1
"inplace/bin/ghc-cabal" copy libraries/Cabal/Cabal dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/Cabal-2.0.1.0
"inplace/bin/ghc-cabal" copy libraries/ghc-boot-th dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/ghc-boot-th-8.2.2
"inplace/bin/ghc-cabal" copy libraries/ghc-boot dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/ghc-boot-8.2.2
"inplace/bin/ghc-cabal" copy libraries/template-haskell dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/template-haskell-2.12.0.0
"inplace/bin/ghc-cabal" copy libraries/hoopl dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/hoopl-3.10.2.2
"inplace/bin/ghc-cabal" copy libraries/transformers dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/transformers-0.5.2.0
"inplace/bin/ghc-cabal" copy libraries/ghc-compact dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/ghc-compact-0.1.0.0
"inplace/bin/ghc-cabal" copy libraries/terminfo dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/terminfo-0.4.1.0
"inplace/bin/ghc-cabal" copy libraries/haskeline dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/haskeline-0.7.4.0
"inplace/bin/ghc-cabal" copy libraries/ghci dist-install "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'  
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/ghci-8.2.2
"inplace/bin/ghc-cabal" copy compiler stage2 "strip" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' 'v p dyn'
Installing library in /opt/local/ghc8.2.2/lib/ghc-8.2.2/ghc-8.2.2
"/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" --force --global-package-db "/opt/local/ghc8.2.2/lib/ghc-8.2.2/package.conf.d" update rts/dist/package.conf.install
Reading package info from "rts/dist/package.conf.install" ... done.
"inplace/bin/ghc-cabal" register libraries/ghc-prim dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for ghc-prim-0.5.1.1..
"inplace/bin/ghc-cabal" register libraries/integer-gmp dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for integer-gmp-1.0.1.0..
"inplace/bin/ghc-cabal" register libraries/base dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for base-4.10.1.0..
"inplace/bin/ghc-cabal" register libraries/filepath dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for filepath-1.4.1.2..
"inplace/bin/ghc-cabal" register libraries/array dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for array-0.5.2.0..
"inplace/bin/ghc-cabal" register libraries/deepseq dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for deepseq-1.4.3.0..
"inplace/bin/ghc-cabal" register libraries/bytestring dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for bytestring-0.10.8.2..
"inplace/bin/ghc-cabal" register libraries/containers dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for containers-0.5.10.2..
"inplace/bin/ghc-cabal" register libraries/time dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for time-1.8.0.2..
"inplace/bin/ghc-cabal" register libraries/unix dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for unix-2.7.2.2..
"inplace/bin/ghc-cabal" register libraries/directory dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for directory-1.3.0.2..
"inplace/bin/ghc-cabal" register libraries/process dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for process-1.6.1.0..
"inplace/bin/ghc-cabal" register libraries/hpc dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for hpc-0.6.0.3..
"inplace/bin/ghc-cabal" register libraries/pretty dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for pretty-1.1.3.3..
"inplace/bin/ghc-cabal" register libraries/binary dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for binary-0.8.5.1..
"inplace/bin/ghc-cabal" register libraries/Cabal/Cabal dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for Cabal-2.0.1.0..
"inplace/bin/ghc-cabal" register libraries/ghc-boot-th dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for ghc-boot-th-8.2.2..
"inplace/bin/ghc-cabal" register libraries/ghc-boot dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for ghc-boot-8.2.2..
"inplace/bin/ghc-cabal" register libraries/template-haskell dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for template-haskell-2.12.0.0..
"inplace/bin/ghc-cabal" register libraries/hoopl dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for hoopl-3.10.2.2..
"inplace/bin/ghc-cabal" register libraries/transformers dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for transformers-0.5.2.0..
"inplace/bin/ghc-cabal" register libraries/ghc-compact dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for ghc-compact-0.1.0.0..
"inplace/bin/ghc-cabal" register libraries/terminfo dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for terminfo-0.4.1.0..
"inplace/bin/ghc-cabal" register libraries/haskeline dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for haskeline-0.7.4.0..
"inplace/bin/ghc-cabal" register libraries/ghci dist-install "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO  
Registering library for ghci-8.2.2..
"inplace/bin/ghc-cabal" register compiler stage2 "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc" "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" "/opt/local/ghc8.2.2/lib/ghc-8.2.2" '' '/opt/local/ghc8.2.2' '/opt/local/ghc8.2.2/lib/ghc-8.2.2' '/opt/local/ghc8.2.2/share/doc/ghc-8.2.2/html/libraries' NO
Registering library for ghc-8.2.2..
for f in '/opt/local/ghc8.2.2/lib/ghc-8.2.2/package.conf.d'/*; do create () { touch "$1" && chmod 644 "$1" ; } && create "$f"; done
"/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin/ghc-pkg" --global-package-db "/opt/local/ghc8.2.2/lib/ghc-8.2.2/package.conf.d" recache
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/bin"
for i in ; do \
	/opt/local/bin/ginstall -c -m 755  $i "/opt/local/ghc8.2.2/bin" ;  \
done
/opt/local/bin/ginstall -c -m 755 -d "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"
for i in inplace/lib/bin/ghc-split; do \
	/opt/local/bin/ginstall -c -m 755  $i "/opt/local/ghc8.2.2/lib/ghc-8.2.2/bin"; \
done
